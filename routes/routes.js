const config = require('../config');
const fs = require('fs');
const {MongoClient, ObjectId} = require('mongodb');
const bcrypt = require('bcrypt');


/*
For the uri change the format below: 
Change the Username to the username i make for you
Juan: jaun_pad
James: james_bush
The password field and the database are fine as they are just change out the user name or you can connect to mine
i also will have to add yor guys IP address to Atlas so you can connect to it from your homes
mongodb+srv://<username>:uJdpYtAE8A.-YuJ@cluster0.rlsc4.mongodb.net/dataexpress?retryWrites=true&w=majority
*/

const url = "mongodb+srv://carlos_duran1:uJdpYtAE8A.-YuJ@cluster0.rlsc4.mongodb.net/dataexpress?retryWrites=true&w=majority"
const client = new MongoClient(url);
//Database things
const dbName = 'dataexpress';
const db = client.db(dbName);
const col = db.collection('data');



exports.index = (req, res) => {
    res.render('Index', {
        title: 'Home',
        config      //config: config ; both are the same so config is all you need
    });
};

exports.orders = (req, res) => {
    res.render('Orders', {
        title: 'Orders',
        config
    });
};

exports.login = (req, res) => {
    res.render('login', {
        title: 'login',
        config
    })
}

exports.create = (req, res) => {
    res.render('create', {
        title: 'create',
        config
    })
}

exports.features = (req, res) => {
    res.render('Features', {
        title: 'Features',
        config
    });
};

exports.dinosaur = (req, res) => {
    let name = req.params.name;
    res.render('Dinosaurs', {
        title: `${name}'s`,
        config
    })
}

const SaltAndHash = pass => {
    let salt = bcrypt.genSaltSync(10);
    let hash = bcrypt.hashSync(pass, salt);
    console.log(hash);
    return hash;
}

exports.log = async (req, res) => {
    let SHpass = SaltAndHash(req.body.password);
    let account = {
        email: req.body.email
    }

    await client.connect();
    let data = {
        pass: SHpass,
        email: req.body.email,
        quest1: req.body.q1,
        quest2: req.body.q2,
        quest3: req.body.q3,
    }
//
    const insertRes = await col.insertOne(data);
    client.close();

    res.render('log', {
        title: 'Login Saved',
        account

    })
}

exports.loggedIn = async (req, res) => {
    let account = {
        email: req.body.email,
    }
    let pass = SaltAndHash(req.body.password);

    await client.connect();
    client.close();

    res.render('loggedin', {
        title: 'Login Accepted',
        account
    })
}

exports.submitted = (req, res) => {
    let person = {
        Nasasaures: req.body.types,
        Features: req.body.kinds,
        fname: req.body.fname,
        lname: req.body.lname,
        email: req.body.email,
        address: req.body.address,
        city: req.body.city,
        state: req.body.state,
        zipcode: req.body.zipcode,
        phoneN: req.body.phonenumber
    };
    let personData = `
        <p>
        Nasasaures: ${person.Nasasaures}<br />
        Features: ${person.Features}<br />
        First Name: ${person.fname}<br />
        Last Name: ${person.lname}<br />
        Email: ${person.email}<br />
        Address: ${person.address}<br />
        City: ${person.city}<br />
        State: ${person.state}<br />
        Zip Code: ${person.zipcode}<br />
        Phone Number: ${person.phoneN}<br />
    ----------------------------------<br />
        </p>
    `;
    
    fs.appendFile('public/submissions.html', personData, err => {
        if(err) throw err;
        console.log('Data has been svaed!')
    });

    res.render('submitted', {
        title: 'Form Accepted',
        person
    })  
};