const express = require('express');
const pug = require('pug');
const path = require('path');
const routes = require('./routes/routes');

const app = express();

app.set('view engine', 'pug');
app.set('views', __dirname + '/views');

app.use(express.static(path.join(__dirname, '/public')));

const urlendcodedParser = express.urlencoded({
    extended: false
});

app.get('/', routes.index);
app.get('/Home', routes.index);
app.get('/Orders', routes.orders);
app.get('/Dinosaurs/:name', routes.dinosaur);
app.get('/Features', routes.features);
app.get('/login', routes.login);
app.get('/create', routes.create);
app.post('/log', urlendcodedParser, routes.log)
app.post('/loggedIn', urlendcodedParser, routes.loggedIn);
app.post('/submitted', urlendcodedParser, routes.submitted);


app.listen(3000);